#ifndef TASK_RANDOM_NUMBER_GENERATOR_H
#define TASK_RANDOM_NUMBER_GENERATOR_H

#include <cstdint>

uint64_t generate_id_event();

#endif  // TASK_RANDOM_NUMBER_GENERATOR_H