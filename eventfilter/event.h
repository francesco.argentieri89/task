#ifndef TASK_EVENT_FILTER_H
#define TASK_EVENT_FILTER_H

#include <chrono>
#include <cstdint>
#include <iomanip>

inline uint32_t mark_timestamp() {
  return std::chrono::duration_cast<std::chrono::microseconds>(
             std::chrono::system_clock::now().time_since_epoch())
      .count();
}

struct Event {
  uint64_t id = 0;
  uint32_t timestamp = 0;
};

inline bool operator==(const Event& lhs, const Event& rhs) {
  return lhs.id == rhs.id && lhs.timestamp == rhs.timestamp;
}

inline bool operator!=(const Event& lhs, const Event& rhs) {
  return !(lhs == rhs);
}

inline std::ostream& operator<<(std::ostream& os, const Event& ev) {
  return os << "ID:" << std::setw(7) << ev.id << " timestamp:" << std::setw(15)
            << ev.timestamp << "\n";
}

struct orderById {
  bool operator()(const Event& lhs, const Event& rhs) {
    return (lhs.id < rhs.id);
  }
};

struct orderByTimeStampDiscendent {
  bool operator()(const Event& lhs, const Event& rhs) {
    return (lhs.timestamp > rhs.timestamp);
  }
};

#endif  // TASK_EVENT_FILTER_H
