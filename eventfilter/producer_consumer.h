//
//  producer_consumer.h
//  Task
//
//  Created by Francesco Argentieri on 05/02/22.
//

#ifndef PRODUCER_CONSUMER_H
#define PRODUCER_CONSUMER_H

#include <chrono>
#include <condition_variable>
#include <mutex>
#include <vector>

#include "event.h"

class EventFilter {
 public:
  EventFilter() = default;
  ~EventFilter() = default;

  void setReadyFlag(bool value) {
    {
      std::unique_lock<std::mutex> lock(mtx_);
      ready_ = value;
    }

    cond_var_.notify_all();
  }

  void produce(std::function<void(std::vector<Event>*)> ptrf,
               std::vector<Event>* p_events) {
    {
      std::unique_lock<std::mutex> lock(mtx_);
      cond_var_.wait(lock, [&] { return ready_; });
    }
    while (ready_) {
      {
        std::unique_lock<std::mutex> lock(mtx_);
        ptrf(p_events);
      }
      cond_var_.notify_all();
    }
  }

  void process(std::function<void(std::vector<Event>*)> ptrf,
               std::vector<Event>* p_events) {
    do {
      {
        std::unique_lock<std::mutex> lock(mtx_);
        cond_var_.wait(lock, [&] { return !p_events->empty(); });
      }
      {
        std::lock_guard<std::mutex> lock(mtx_);
        ptrf(p_events);
      }
    } while (ready_);
  }

 private:
  std::mutex mtx_;
  std::condition_variable cond_var_;
  bool ready_{false};
};

#endif  // PRODUCER_CONSUMER_H
