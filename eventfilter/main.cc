
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include "event.h"
#include "number_generator.h"
#include "producer_consumer.h"

std::condition_variable cond_var;
std::mutex mutex_var;
bool ready;

uint64_t removed;
uint64_t sorted;
uint64_t sorted_by_id;
uint64_t sorted_by_time;
uint64_t added;
uint64_t manipulated;

constexpr std::size_t kFirstFiveElements{5};

static void monitor(const std::vector<Event>& vec) {
  while (true) {
    std::unique_lock<std::mutex> lock(mutex_var);
    cond_var.wait(lock, [] { return ready; });
    if (!vec.empty()) {
      std::cout << "total found events: " << vec.size() << std::endl;
      std::cout << "event in vector (show only 5): " << std::endl;
      // prints the first five elements of the vector
      if (vec.size() > 5) {
        for (std::size_t i = 0; i < kFirstFiveElements; i++) {
          std::cout << " - " << vec.at(i);
        }
        std::cout << " ..." << std::endl;
      }
    } else {
      std::cout << "Looking forward to capturing events\n";
    }

    std::cout << std::string(80, '-') << "\n\n";
  }
}

// simple function that filter element not divisible by 17
static void remove_event(std::vector<Event>* p_vec) {
  auto previous = p_vec->size();
  p_vec->erase(std::remove_if(p_vec->begin(), p_vec->end(),
                              [](const Event& ev) { return ev.id % 17 == 0; }),
               p_vec->end());
  if (previous - p_vec->size() > 0) {
    removed++;
  }
}

static void manipulate_id_events(std::vector<Event>* p_vec) {
  auto it = std::find_if(p_vec->begin(), p_vec->end(),
                         [](const auto& e) { return (e.id % 3 == 0); });

  if (it != p_vec->end()) {
    it->id += 13;
    it->timestamp = mark_timestamp();
    manipulated++;
  }
}

// sort elements in vector by ID or by TimeStamp
static void sort_events(std::vector<Event>* p_vec) {
  static uint64_t counter;
  if (counter % 11 == 0) {
    static orderById orderByIdAscendent;
    std::sort(p_vec->begin(), p_vec->end(), orderByIdAscendent);
    sorted_by_id++;
  } else {
    static orderByTimeStampDiscendent orderDiscendent;
    std::sort(p_vec->begin(), p_vec->end(), orderDiscendent);
    sorted_by_time++;
  }
  counter++;
}

// append event in vector
static void add_event(std::vector<Event>* p_vec) {
  Event ev;
  ev.id = generate_id_event();
  ev.timestamp = mark_timestamp();
  p_vec->push_back(ev);
  added++;
}

static void event_filter(std::vector<Event>* p_vec) {
  {
    // barrier wait start signal
    std::unique_lock<std::mutex> lock(mutex_var);
    std::cout << "is waiting to start\n";
    cond_var.wait(lock);
  }

  while (ready) {  // infinity loop
    // add event to vector
    {
      mutex_var.lock();
      add_event(p_vec);
      mutex_var.unlock();
    }

    // sort events
    {
      std::lock_guard<std::mutex> lock(mutex_var);
      sort_events(p_vec);
    }

    // manipulate id
    {
      std::lock_guard<std::mutex> lock(mutex_var);
      manipulate_id_events(p_vec);
    }

    // remove element by filter setted at 17
    {
      mutex_var.lock();
      remove_event(p_vec);
      mutex_var.unlock();
    }
  }
}

int main(int argc, char* argv[]) {
  std::cout << "Task -- event filter" << std::endl;
  std::cout << std::string(80, '-') << "\n\n\n";
  const auto processor_count = std::thread::hardware_concurrency();
  // clang-format off
  std::cout << "found n. processor:" << std::setw(3) << processor_count << std::endl;
  // clang-format on

  // set zero counters
  removed = 0;
  sorted = 0;
  added = 0;
  manipulated = 0;
  sorted_by_id = 0;
  sorted_by_time = 0;

  // prepare event vector
  std::vector<Event> vec;
  vec.reserve(200000);

  std::vector<std::thread> thread_filter;
  thread_filter.reserve(processor_count);

  // set ready (true - start infinite loops) the filter and the monitor
  ready = true;
  for (auto i = 0U; i < processor_count / 2; i++) {
    std::thread task(event_filter, &vec);
    thread_filter.push_back(std::move(task));
  }

  // create thread for monitor
  std::thread(monitor, std::cref(vec)).detach();
  std::chrono::seconds duration(5);
  std::this_thread::sleep_for(duration);

  // start all threads
  cond_var.notify_all();

  // trace activities for x seconds
  std::chrono::seconds trace_activities_duration(60);
  std::this_thread::sleep_for(trace_activities_duration);

  // set ready (false - stop infinite loops) the filter and the monitor
  ready = false;
  for (auto& th : thread_filter) {
    if (th.joinable()) {
      th.join();
    } else {
      std::cout << "not joinable\n";
    }
  }

  sorted = sorted_by_id + sorted_by_time;
  {
    std::lock_guard<std::mutex> lk(mutex_var);
    // clang-format off
    std::cout << "\n\n--- Recap:\n";
    std::cout << "found n. processor:" << std::setw(3) << processor_count << std::endl;
    std::cout << "events:\n";
    std::cout << " - after the tracking period:" << std::setw(7) << vec.size() << std::endl; 
    std::cout << " - added:" << std::setw(27) << added << std::endl; 
    std::cout << " - manipulated:" << std::setw(21) << manipulated << std::endl; 
    std::cout << " - removed:" << std::setw(25) << removed << std::endl; 
    std::cout << " - sorted:" << std::setw(26) << sorted << "\t" 
              << " by id:" << std::setw(3) << sorted_by_id
              << " by timestamp:" << std::setw(3) << sorted_by_time << std::endl;
    // clang-format on
    vec.clear();
  }

  auto prev_removed = removed;
  auto prev_sorted = sorted_by_id + sorted_by_time;
  auto prev_added = added;
  auto prev_manipulated = manipulated;
  std::cout << "\n\n****************************************\n\n";
  ///////////////////////////////////////////////////////////////////////////
  // --- producer-consumer approach
  ///////////////////////////////////////////////////////////////////////////
  ready = false;
  // set zero counters
  removed = 0;
  sorted = 0;
  added = 0;
  manipulated = 0;
  sorted_by_id = 0;
  sorted_by_time = 0;

  // initializ event-filter
  EventFilter events;
  events.setReadyFlag(ready);

  std::thread AddEvent(
      [&events](std::vector<Event>* ptr_vec) {
        events.produce(add_event, ptr_vec);
      },
      &vec);
  std::thread ManipulateIdEvent(
      [&events](std::vector<Event>* ptr_vec) {
        events.process(manipulate_id_events, ptr_vec);
      },
      &vec);
  std::thread SortEvent(
      [&events](std::vector<Event>* ptr_vec) {
        events.process(sort_events, ptr_vec);
      },
      &vec);
  std::thread RemoveEvent(
      [&events](std::vector<Event>* ptr_vec) {
        events.process(remove_event, ptr_vec);
      },
      &vec);
  // wait five seconds
  std::this_thread::sleep_for(duration);
  {
    std::lock_guard<std::mutex> lk(mutex_var);
    ready = true;
    events.setReadyFlag(ready);
  }
  // start all threads
  cond_var.notify_all();

  // trace activities for x seconds
  std::this_thread::sleep_for(trace_activities_duration);
  {
    std::lock_guard<std::mutex> lk(mutex_var);
    ready = false;
    events.setReadyFlag(ready);
  }

  if (AddEvent.joinable()) {
    AddEvent.join();
  } else {
    std::cerr << "not joinable\n";
  }

  if (ManipulateIdEvent.joinable()) {
    ManipulateIdEvent.join();
  } else {
    std::cerr << "not joinable\n";
  }

  if (SortEvent.joinable()) {
    SortEvent.join();
  } else {
    std::cerr << "not joinable\n";
  }

  if (RemoveEvent.joinable()) {
    RemoveEvent.join();
  } else {
    std::cerr << "not joinable\n";
  }
  std::lock_guard<std::mutex> lk(mutex_var);
  sorted = sorted_by_id + sorted_by_time;

  // clang-format off
    std::cout << "\n\n--- Comparing Recap:\n";
    std::cout << "found n. processor:" << std::setw(3) << processor_count << std::endl;
    std::cout << "events:\n";
    std::cout << " - after the tracking period:" << std::setw(7) << vec.size() << std::endl;
    std::cout << " - added:" << std::setw(27) << added << std::endl;
    std::cout << " - manipulated:" << std::setw(21) << manipulated << std::endl;
    std::cout << " - removed:" << std::setw(25) << removed << std::endl;
    std::cout << " - sorted:" << std::setw(26) << sorted << "\t"
              << " by id:" << std::setw(3) << sorted_by_id
              << " by timestamp:" << std::setw(3) << sorted_by_time << std::endl;
  // clang-format on
  vec.clear();

  std::cout << "\n\n.....................................................\n";
  std::cout << "   Producer Consumer   |    Fucntional  \n";
  // clang-format off
  std::cout << " - added:" << std::setw(6 + 7) << added <<" | " << std::setw(7) << prev_added << std::endl;
  std::cout << " - manipulated:" << std::setw(7) << manipulated <<" | " << std::setw(7) << prev_manipulated << std::endl;
  std::cout << " - removed:" << std::setw(4 + 7) << removed <<" | " << std::setw(7) << prev_removed << std::endl;
  std::cout << " - sorted:" << std::setw(5 + 7) << sorted <<" | " << std::setw(7) << prev_sorted << std::endl;
  // clang-format on
  return 0;
}
