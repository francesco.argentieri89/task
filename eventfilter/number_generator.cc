#include "number_generator.h"

#include <random>

uint64_t generate_id_event() {
  static std::random_device rd;
  static thread_local std::default_random_engine re(rd());
  std::uniform_int_distribution<uint64_t> rdist(500, 700);

  return rdist(re);
}
