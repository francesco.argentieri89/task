# SynSense Interview Take Home Task

The task specified in this readme is designed to evaluate how you design and write code.
It should take between one and four hours.

## General Notes

- You can use whichever version of C++ you prefer.
- The solution should compile on Ubuntu 20.04
- You can use a build system of your choice
- The solution should be uploaded as a git repository to gitlab.com

## An Event Filtering System

The goal is to create an event filtering system which can apply a given processing
function to a collection of events. Events consist of an ID and a time stamp, and are
defined as:

```cpp
struct Event {
   uint64_t id = 0;
   uint32_t timestamp = 0;
}
```

A collection of events can be an `std::vector<Event>`.

Processing functions could for example:

- Transform event IDs and timestamps.
- Add events to the collection.
- Remove events to the collection based on a predicate.

The output of a filter is the entire collection after the processing function has been executed.
It should be possible to connect the output of a filter to the inputs of multiple other filters, the input of a filter could be a queue for example.

Filters should be able to run in separate threads.

Provide a main function which shows off your design.

## Build and execution

- require Cmake >= 3.18

```sh
mkdir build && cd build
cmake ..
cmake --build . && ./eventfilter/task
```
